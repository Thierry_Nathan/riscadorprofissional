﻿namespace RISCADORPROFISSIONAL
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxIncremento = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxTamanho = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxAngulo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxLinhas = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.canvas = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.textBoxIncremento);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.textBoxTamanho);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.textBoxAngulo);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBoxLinhas);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(603, 59);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(533, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(67, 41);
            this.button1.TabIndex = 0;
            this.button1.Text = "GO!";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.buttonGo_Click);
            // 
            // textBoxIncremento
            // 
            this.textBoxIncremento.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxIncremento.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxIncremento.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxIncremento.Location = new System.Drawing.Point(478, 23);
            this.textBoxIncremento.Name = "textBoxIncremento";
            this.textBoxIncremento.Size = new System.Drawing.Size(49, 17);
            this.textBoxIncremento.TabIndex = 3;
            this.textBoxIncremento.Text = "1";
            this.textBoxIncremento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(393, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 16);
            this.label4.TabIndex = 2;
            this.label4.Text = "Incremento:";
            // 
            // textBoxTamanho
            // 
            this.textBoxTamanho.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxTamanho.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxTamanho.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTamanho.Location = new System.Drawing.Point(338, 23);
            this.textBoxTamanho.Name = "textBoxTamanho";
            this.textBoxTamanho.Size = new System.Drawing.Size(49, 17);
            this.textBoxTamanho.TabIndex = 3;
            this.textBoxTamanho.Text = "5";
            this.textBoxTamanho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(269, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tamanho:";
            // 
            // textBoxAngulo
            // 
            this.textBoxAngulo.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxAngulo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxAngulo.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAngulo.Location = new System.Drawing.Point(214, 23);
            this.textBoxAngulo.Name = "textBoxAngulo";
            this.textBoxAngulo.Size = new System.Drawing.Size(49, 17);
            this.textBoxAngulo.TabIndex = 3;
            this.textBoxAngulo.Text = "74";
            this.textBoxAngulo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(159, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ângulo:";
            // 
            // textBoxLinhas
            // 
            this.textBoxLinhas.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBoxLinhas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxLinhas.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxLinhas.Location = new System.Drawing.Point(104, 23);
            this.textBoxLinhas.Name = "textBoxLinhas";
            this.textBoxLinhas.Size = new System.Drawing.Size(49, 17);
            this.textBoxLinhas.TabIndex = 1;
            this.textBoxLinhas.Text = "100";
            this.textBoxLinhas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nº de linhas:";
            // 
            // canvas
            // 
            this.canvas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.canvas.Location = new System.Drawing.Point(0, 59);
            this.canvas.Name = "canvas";
            this.canvas.Size = new System.Drawing.Size(603, 391);
            this.canvas.TabIndex = 0;
            this.canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.canvas_Paint);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 450);
            this.Controls.Add(this.canvas);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "RISCADOR PRO 1.0.1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxIncremento;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxTamanho;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxAngulo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxLinhas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel canvas;
    }
}

